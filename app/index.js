/** @jsx React.DOM */

var React = require('react')
window.React = React
var App = require('./components/App')

React.renderComponent(<App />, document.body)

// On boot - check hash state
var qs = require('qs2')
var QueryActions = require('./actions/QueryActions')
if (window.location.hash) {
  var hash = window.location.hash.replace('#', '')
  var params = qs.parse(hash)
  QueryActions.setParams(params)
  QueryActions.submit()
}
