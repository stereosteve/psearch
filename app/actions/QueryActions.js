var Reflux = require('reflux')

var QueryActions = Reflux.createActions([
  "setParam",
  "setParams",
  "submit"
]);

module.exports = QueryActions
