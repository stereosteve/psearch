/** @jsx React.DOM */


var SquareImg = React.createClass({
  propTypes: {
      src: React.PropTypes.string.isRequired,
      size: React.PropTypes.number.isRequired
  },
  render: function () {
    var style = {
      'border': '1px solid #ccc',
      'width': this.props.size,
      'height': this.props.size,
      'background-size': 'contain',
      'background-position': 'center',
      'background-repeat': 'no-repeat',
      'background-color': 'white',
      'background-image': 'url(' + this.props.src + ')'
    }
    return this.transferPropsTo(<div style={style} />)
  }
})


module.exports = SquareImg;
