/**
 * @jsx React.DOM
 */

var React = require('react');
var ResponseStore = require('../stores/ResponseStore')
var PriceTable = require('./PriceTable')



var ResponsePanel = React.createClass({
  _getState: function() {
    return {
      response: ResponseStore.response
    }
  },
  _onChange: function() {
    this.setState(this._getState())
  },
  getInitialState: function () {
    return this._getState()
  },
  componentDidMount: function() {
    this.unsubscribe = ResponseStore.listen(this._onChange);
  },
  componentWillUnmount: function() {
    this.unsubscribe();
  },
  render: function() {
    if (ResponseStore.isLoading) return <div>loading...</div>
    return <PriceTable resp={this.state.response} />;
  }
});

module.exports = ResponsePanel;
