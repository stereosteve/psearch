/** @jsx React.DOM */
var React = require('react');
var _ = require('lodash')
var formatNumber = require('format-number')()

//
// PriceTable
//
var PriceTable = React.createClass({
  render: function() {
    var resp = this.props.resp
    if (!resp) return null

    var settings = this.props.settings || {};
    var tbodys = resp.results.map(function(r) {
      return <PartTbody key={r.item.uid} part={r.item} settings={settings} />
    })

    return (
      <table className="PriceTable">
        {tbodys}
      </table>
    )
  }
})



//
// PartTbody
//
var PartTbody = React.createClass({
  getInitialState: function() {
    return {
      expanded: false
    }
  },
  toggleExpanded: function() {
    this.setState({
      expanded: !this.state.expanded
    })
  },
  render: function() {
    var part = this.props.part
    var settings = this.props.settings
    var breaks = settings.price_breaks || [1, 10, 100, 1000, 10000]

    var headerCells = ['', 'Distributer', 'SKU', 'Stock', 'MOQ', 'Pkg', ''].map(function(col, i) {
      return <th key={'col'+i} data-col-header={col}>{col}</th>
    })
    breaks.forEach(function(brk) {
      brk = formatNumber(brk)
      headerCells.push(<th key={'brk'+brk} className="priceAtBreak">{brk}</th>)
    })

    // group multiple offers under disty
    var distyMap = {}
    var distyOffers = _.groupBy(part.offers, function(o) { return o.seller.name })
    _.each(distyOffers, function(offers, distyName) {
      var disty = distyMap[distyName] = {}
      var offer = offers[0]
      var currencies = _.keys(offer.prices)
      var currency = currencies[0]
      var priceTuples = offer.prices[currency]
      if (priceTuples) {
        disty.display_currency = currency
        disty.display_quantity = priceTuples[0][0]
        disty.display_price = priceTuples[0][1]
      }
      disty.name = distyName
      disty.offers = offers
      disty.is_authorized = offer.is_authorized
    })

    var distyValues = _.values(distyMap)

    // sort by settings.preferred_vendors
    if (settings.preferred_distributors) {
      distyValues = _.sortBy(distyValues, function(d, i) {
        var pos = settings.preferred_distributors.indexOf(d.name)
        if (pos > -1) {
          return pos
        } else {
          return settings.preferred_distributors.length + i
        }
      })
    }

    // take the first n offers
    var toggleExpandedText
    if (!this.state.expanded) {
      toggleExpandedText = 'Show All ('+distyValues.length+')'
      distyValues = _.first(distyValues, settings.number_of_offers_to_preview || 5)
    } else {
      toggleExpandedText = 'Show Fewer'
    }

    var offerRows = distyValues.map(function(disty, i) {
      return <PartOfferRow key={i} disty={disty} breaks={breaks} i={i} />
    })

    return (
      <tbody className="PartTbody">

        <tr className="PartTbody-top">
          <td colSpan={headerCells.length}>
            <PartHeader part={part} />
          </td>
        </tr>

        <tr className="PartTbody-headings">
          {headerCells}
        </tr>

        {offerRows}

        <tr className="PartTbody-bottom">
          <td colSpan={headerCells.length}>
            <div className="text-center">
              <div className="btn btn-link btn-sm" onClick={this.toggleExpanded}>
                {toggleExpandedText}
              </div>
            </div>
            <PartFooter part={part} />
          </td>
        </tr>

      </tbody>
    )
  }
})

var PartHeader = React.createClass({
  render: function() {
    var part = this.props.part
    var anImg = part.imagesets[0]
    var imgSrc = anImg && anImg.small_image && anImg.small_image.url
    return (
      <div className="PartHeader media">
        <div className="pull-left">
          <img width="50px" className="media-object" src={imgSrc}/>
        </div>
        <div className="media-body">
          <h3 className="media-heading">
            {part.brand.name}
            {' '}
            <span className="text-muted">{part.mpn}</span>
          </h3>
          <p className="text-muted small">{part.short_description}</p>
        </div>
      </div>
    )
  }
})

var PartOfferRow = React.createClass({
  getInitialState: function() {
    return {}
  },
  setSelectedSku: function(val) {
    this.setState({
      selectedSku: val
    })
  },
  render: function() {
    var disty = this.props.disty
    var breaks = this.props.breaks
    var trClassName = this.props.i % 2 == 0 ? 'PartOfferRow-even' : 'PartOfferRow-odd'

    // skus
    var skus = _.chain(disty.offers).pluck('sku').uniq().value()
    var selectedSku = this.state.selectedSku || _.first(skus)
    var offer = _.findWhere(disty.offers, {sku: selectedSku})

    // Currency Selector
    var currencyList = _.keys(offer.prices)
    // TODO: settings.preferred_currency
    var selectedCurrency = _.first(currencyList)
    var prices = offer.prices[selectedCurrency] || []
    prices = prices.concat().reverse()
    function getPriceAtQuantity(q) {
      var priceTuple = _.find(prices, function(tuple) {
        return q >= tuple[0];
      })
      if (!priceTuple) return
      return formatNumber(priceTuple[1])
    }

    var priceAtBreaks = breaks.map(function(q) {
      var key = [selectedSku, selectedCurrency, q].join('-')
      return <td key={key} className="priceAtBreak">{getPriceAtQuantity(q)}</td>
    })

    return (
      <tr className={trClassName}>
        <td>{offer.is_authorized ? '*' : ''}</td>
        <td>
          <a href={offer.product_url}>{offer.seller.name}</a>
        </td>
        <td>
          <SkuSelect values={skus} selectedSku={selectedSku} setSelectedSku={this.setSelectedSku}/>
        </td>
        <td className="text-right">{formatNumber(offer.in_stock_quantity)}</td>
        <td className="text-right">{formatNumber(offer.moq)}</td>
        <td className="colPkg text-right">{offer.packaging}</td>
        <td className="text-center">
          {selectedCurrency}
        </td>
        {priceAtBreaks}
      </tr>
    )

  }
})

var PartFooter = React.createClass({

  render: function() {
    var part = this.props.part
    return (
      <div className="PartFooter">
        <a className="pull-right" href="javascript:;">Visit Manufactorer <span className="caret"></span></a>

        <a href="javascript:;">
          <i className="fa fa-info-circle"></i> See Details
        </a>
        <a href="javascript:;"><i className="fa fa-dollar"></i> Compare Prices</a>
        <PartFavoriteButton part={part}/>
        <a href="javascript:;"><i className="fa fa-plus"></i> Save to BOM</a>
        <a href="javascript:;"><i className="fa fa-envelope-o"></i> Email</a>
      </div>
    )
  }
})

var PartFavoriteButton = React.createClass({
  // TODO: handle favorite stuff
  getInitialState: function() {
    return {
      isFavorite: false
    }
  },
  toggleFavorite: function() {
    debugger
  },
  render: function() {
    var style = {}
    var text = 'Add to Favorites'
    if (this.state.isFavorite) {
      style.color = 'firebrick'
      text = 'Favorite'
    }
    return (
      <a href="javascript:;" style={style} onClick={this.toggleFavorite}>
        <i className="fa fa-heart"></i>{' ' + text}
      </a>
    )
  }
})



// SKU select
var SkuSelect = React.createClass({
  handleChange: function(event) {
    this.props.setSelectedSku(event.target.value)
  },
  render: function() {
    var values = this.props.values
    var selectedSku = this.props.selectedSku || values[0]
    if (values.length < 2) {
      return <div>{selectedSku}</div>
    }

    var options = values.map(function(v) {
      return <option key={v} value={v}>{v}</option>
    })
    return (
      <select value={selectedSku} onChange={this.handleChange}>
        {options}
      </select>
    );
  }
})



module.exports = PriceTable
