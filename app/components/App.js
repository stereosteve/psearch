/** @jsx React.DOM */
var React = require('react')
var QueryPanel = require('./QueryPanel')
var ResponsePanel = require('./ResponsePanel')

var App = React.createClass({
  render: function () {
    return <div>
      <QueryPanel />
      <ResponsePanel />
    </div>
  }
})


module.exports = App
