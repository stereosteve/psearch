/** @jsx React.DOM */

var React = require('react')
var QueryActions = require('../actions/QueryActions')
var QueryStore = require('../stores/QueryStore')



var QueryPanel = React.createClass({
  _getState: function() {
    return {
      params: QueryStore.params
    }
  },
  _onChange: function() {
    this.setState(this._getState());
  },
  _handleQueryChange: function(ev) {
    var q = ev.target.value
    QueryActions.setParam('q', q)
  },
  _handleSubmit: function(ev) {
    ev.preventDefault()
    QueryActions.submit()
  },
  getInitialState: function () {
    return this._getState()
  },
  componentDidMount: function() {
    this.unsubscribe = QueryStore.listen(this._onChange);
  },
  componentWillUnmount: function() {
    this.unsubscribe();
  },
  render: function () {
    var params = this.state.params
    return <div className="TopSearch">
      <form onSubmit={this._handleSubmit} className="form-group">
        <input type="text"
          className="form-control"
          value={params.q}
          onChange={this._handleQueryChange}/>
      </form>
    </div>
  }
})





module.exports = QueryPanel
