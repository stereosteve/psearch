var Reflux = require('reflux')
var QueryActions = require('../actions/QueryActions')
var QueryStore = require('./QueryStore')
var http = require('httpplease')
var qs = require('qs2')
var _ = require('lodash')

var ResponseStore = Reflux.createStore({

  init: function() {
    this.listenTo(QueryActions.submit, this.fetch)
  },

  fetch: function () {

    var params = _.clone(QueryStore.params)
    window.location.hash = qs.stringify(params)

    _.assign(params, {
      'include': ['imagesets', 'short_description'],
      'slice[imagesets]': '[:1]',
      'facet': {
        'fields': {
          'brand.name': {
            include: true,
            exclude_filter: true,
            limit: 6
          },
          'offers.seller.name': {
            include: true,
            exclude_filter: true,
            limit: 6
          }
        }
      },
      'spec_drilldown[include]': true,
      'spec_drilldown[exclude_filter]': true,
      'apikey': '68b25f31'
    })

    var url = '/api/v3/parts/search?'+qs.stringify(params)

    this.isLoading = true
    this.trigger()
    http.get(url, function(err, r) {
      if (err) throw(err)
      var resp = JSON.parse(r.text)
      this.isLoading = false
      this.response = resp
      this.trigger()
    }.bind(this))
  }

})


module.exports = ResponseStore
