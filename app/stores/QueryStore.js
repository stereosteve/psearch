var Reflux = require('reflux')
var QueryActions = require('../actions/QueryActions')

var QueryStore = Reflux.createStore({

  init: function () {
    this.params = {}
    this.listenTo(QueryActions.setParam, this.setParam)
    this.listenTo(QueryActions.setParams, this.setParams)
  },

  setParam: function(param, val) {
    this.params[param] = val
    this.trigger()
  },

  setParams: function(params) {
    this.params = params
    this.trigger()
  }

})

module.exports = QueryStore
