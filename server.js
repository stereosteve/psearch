var express = require('express')
var proxy = require('proxy-middleware')
var url = require('url')

var app = express()
app.use(express.static(__dirname + '/public'))
app.use('/api', proxy(url.parse('http://octopart.com/api')));

var port = process.env.PORT || 3003
app.listen(port)
console.log(':' + port)
